#!/usr/bin/env ruby
# encoding: UTF-8

don_t_read_files = [
  "src/http/modules/perl/",
  "src/http/modules/ngx_http_addition_filter_module.c",
  "src/http/modules/ngx_http_memcached_module.c",
  "src/http/modules/ngx_http_ssi_filter_module.h",
  "src/http/modules/ngx_http_ssi_filter_module.c",
  "src/os/unix/ngx_freebsd",
  "src/os/unix/ngx_sunpro",
  "src/os/unix/ngx_solaris",
  "src/os/unix/ngx_darwin",
  "src/http/modules/ngx_http_gunzip_filter_module.c",
  "src/http/modules/ngx_http_scgi_module.c",
  "src/http/modules/ngx_http_xslt_filter_module.c",
  "src/http/modules/ngx_http_charset_filter_module.c",
  "src/event/modules/",
  "src/core/ngx_sha1",
  "src/core/ngx_crc",
  "src/core/nginx",
  "src/core/ngx_array",
  "src/core/ngx_buf",
]

has_read_files = IO.popen("git diff --numstat source | awk '{print $3}'").readlines.map do |f|
  f.strip
end
to_read_files = IO.popen("git ls-files | grep '^src/\\(http\\|os/unix\\|stream\\|core\\|event\\)' | grep '.*\\.c$'").readlines
to_read_file_num = 0
has_read_file_num = 0
to_read_files.each do |f|
  f.strip!
  if has_read_files.include?(f)
    puts "\e[32;1m#{f} OK\e[0m"
    has_read_file_num += 1
  else
    if !don_t_read_files.any? {|prefix| f.start_with? prefix }
      puts "\e[31;1m#{f} TODO\e[0m"
      to_read_file_num += 1
    end
  end
end

puts "Done: #{has_read_file_num} TODO: #{to_read_file_num}"
