
/*
 * Copyright (C) Igor Sysoev
 * Copyright (C) Nginx, Inc.
 */


#ifndef _NGX_EVENT_PIPE_H_INCLUDED_
#define _NGX_EVENT_PIPE_H_INCLUDED_


#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_event.h>


typedef struct ngx_event_pipe_s  ngx_event_pipe_t;

typedef ngx_int_t (*ngx_event_pipe_input_filter_pt)(ngx_event_pipe_t *p,
                                                    ngx_buf_t *buf);
typedef ngx_int_t (*ngx_event_pipe_output_filter_pt)(void *data,
                                                     ngx_chain_t *chain);


struct ngx_event_pipe_s {
    ngx_connection_t  *upstream;
    ngx_connection_t  *downstream;

    // 直接来自上游的响应。后接收的响应在前，先接收的响应在后。
    ngx_chain_t       *free_raw_bufs;
    // 接收到上游响应后，由 input_filter 设置到 in 链表中
    ngx_chain_t       *in;
    // 指向 in 上最新收到的缓冲区
    ngx_chain_t      **last_in;

    // 通过 thread 的方式正在写入临时文件的缓冲区链表
    // 未来可能会支持 aio
    // @see aio_write
    ngx_chain_t       *writing;

    // 写入文件成功时，会把已经写入 in 链表添加 out 链表中
    ngx_chain_t       *out;
    // 等待释放的缓冲区
    ngx_chain_t       *free;
    // 表示上次调用 output_filter 方法发送响应时没有发送的 out 链表
    // 它是 out 链表的一部分
    ngx_chain_t       *busy;

    /*
     * the input filter i.e. that moves HTTP/1.1 chunks
     * from the raw bufs to an incoming chain
     */

    ngx_event_pipe_input_filter_pt    input_filter;
    void                             *input_ctx;

    ngx_event_pipe_output_filter_pt   output_filter;
    void                             *output_ctx;

#if (NGX_THREADS)
    ngx_int_t                       (*thread_handler)(ngx_thread_task_t *task,
                                                      ngx_file_t *file);
    void                             *thread_ctx;
    ngx_thread_task_t                *thread_task;
#endif

    // 已经读到上游的响应
    unsigned           read:1;
    // 是否走缓存相关逻辑
    unsigned           cacheable:1;
    // 接收上游响应时一次是否只能接收一个缓冲区
    unsigned           single_buf:1;
    // 是否在缓冲区没有被引用时回收缓冲区
    unsigned           free_bufs:1;
    // 可以结束跟上游的链接了
    unsigned           upstream_done:1;
    unsigned           upstream_error:1;
    unsigned           upstream_eof:1;
    // 暂时阻塞上游的响应，准备腾出接收响应的缓冲区
    unsigned           upstream_blocked:1;
    unsigned           downstream_done:1;
    unsigned           downstream_error:1;
    // 是否复用临时文件空间
    unsigned           cyclic_temp_file:1;
    // aio 写是否启用
    unsigned           aio:1;

    ngx_int_t          allocated;
    ngx_bufs_t         bufs;
    ngx_buf_tag_t      tag;

    ssize_t            busy_size;

    off_t              read_length;
    off_t              length;

    off_t              max_temp_file_size;
    ssize_t            temp_file_write_size;

    ngx_msec_t         read_timeout;
    ngx_msec_t         send_timeout;
    // 向下游发送响应时，TCP 连接中设置的 send_lowat
    // 跟 BSD 的 SO_SNDLOWAT socket 选项相关，对其他平台没有意义
    ssize_t            send_lowat;

    ngx_pool_t        *pool;
    ngx_log_t         *log;

    // 在接收响应报头阶段，接收到的响应体
    ngx_chain_t       *preread_bufs;
    // 在接收响应报头阶段，接收到的响应体长度
    size_t             preread_size;
    ngx_buf_t         *buf_to_file;

    // 这里是proxy/xxxcgi_cache 的 limit_rate
    size_t             limit_rate;
    time_t             start_sec;

    ngx_temp_file_t   *temp_file;

    /* STUB */ int     num;
};


ngx_int_t ngx_event_pipe(ngx_event_pipe_t *p, ngx_int_t do_write);
ngx_int_t ngx_event_pipe_copy_input_filter(ngx_event_pipe_t *p, ngx_buf_t *buf);
ngx_int_t ngx_event_pipe_add_free_buf(ngx_event_pipe_t *p, ngx_buf_t *b);


#endif /* _NGX_EVENT_PIPE_H_INCLUDED_ */
